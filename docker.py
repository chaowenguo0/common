import aiohttp, asyncio, tarfile, io, argparse, pathlib, sys, builtins
parser = argparse.ArgumentParser()
parser.add_argument('image') #'library/ubuntu'
parser.add_argument('sha256')
parser.add_argument('swith')
parser.add_argument('target', nargs='+')
args = parser.parse_args()
directory = pathlib.Path(__file__).resolve().parent.joinpath(args.image.split('/')[0])
directory.mkdir()

def generator(members):
    for tarinfo in members:
        for target in args.target:
            if builtins.getattr(tarinfo.name, args.swith + 'swith')(target):
                tarinfo.name = tarinfo.name.split(target)[1] if args.swith == 'start' else target
                yield tarinfo

async def main():
    async with aiohttp.ClientSession() as session:
        async with session.get(f'https://auth.docker.io/token?service=registry.docker.io&scope=repository:{args.image}:pull') as response:
            token = (await response.json()).get('token')
            async with session.get(f'https://registry-1.docker.io/v2/{args.image}/manifests/sha256:{args.sha256}', headers={'authorization':f'Bearer {token}', 'accept':'application/vnd.docker.distribution.manifest.v2+json, application/vnd.oci.image.manifest.v1+json'}) as manifests:
                for _ in (await manifests.json()).get('layers'):
                    async with session.get(f'https://registry-1.docker.io/v2/{args.image}/blobs/{_.get("digest")}', headers={'authorization':f'Bearer {token}'}) as response:
                        tar = tarfile.open(mode='r:gz', fileobj=io.BytesIO(await response.content.read()))
                        tar.extractall(directory, sys.modules[__name__].generator(tar))
                    if args.swith == 'end' and builtins.sum(1 for _ in directory.iterdir()) == builtins.len(args.target): break
                    await asyncio.sleep(1)

asyncio.run(main())