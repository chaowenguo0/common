import aiohttp, asyncio, datetime, hmac, hashlib, base64, itertools, builtins, sys, pathlib, argparse, urllib.parse

parser = argparse.ArgumentParser()
for _ in ('share', 'file'): parser.add_argument(_)
args = parser.parse_args()
storage = 'cs410032001172b072a'
accountKey = 'ckgqvtQAVaT2yvbq9nhzkYRcOaG6KAWv84agcL/HqiJY/mAPFi/r81a9Za7jiHgJb8GtSMBR3kdN+AStMMfBwQ=='
version = '2021-08-06'
date = datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
data = pathlib.Path(__file__).resolve().parent.joinpath(args.file).read_bytes()
length = builtins.len(data)

async def main():
    async with aiohttp.ClientSession() as session:
        headers = {'x-ms-content-length':builtins.str(length), 'x-ms-date':date, 'x-ms-type':'file', 'x-ms-version':version}
        signed = base64.b64encode(hmac.new(base64.b64decode(accountKey.encode()), msg=''.join(itertools.chain('PUT', itertools.repeat('\n', 12), (f'{key}:{value}\n' for key,value in headers.items()), f'/{storage}/{args.share}/{urllib.parse.quote(args.file)}')).encode(), digestmod=hashlib.sha256).digest()).decode()
        url = f'https://{storage}.file.core.windows.net/{args.share}/{args.file}'
        async with session.put(url, headers={**headers, 'content-type':'', 'authorization':f'SharedKey {storage}:{signed}'}) as _: pass
        headers = {'x-ms-date':date, 'x-ms-range':f'bytes=0-{builtins.str(length - 1)}', 'x-ms-version':version, 'x-ms-write':'update'}
        signed = base64.b64encode(hmac.new(base64.b64decode(accountKey.encode()), msg=''.join(itertools.chain('PUT', itertools.repeat('\n', 3), builtins.str(length), itertools.repeat('\n', 9), (f'{key}:{value}\n' for key,value in headers.items()), f'/{storage}/{args.share}/{urllib.parse.quote(args.file)}\ncomp:range')).encode(), digestmod=hashlib.sha256).digest()).decode()
        async with session.put(url, params={'comp':'range'}, headers={**headers, 'content-length':builtins.str(length), 'content-type':'', 'authorization':f'SharedKey {storage}:{signed}'}, data=data) as _: pass

asyncio.run(sys.modules[__name__].main()) 