import asyncio, os, pathlib

async def main():
    proc = await asyncio.create_subprocess_exec('docker', 'run', '--rm', 'python:slim', 'python', '-c', 'import platform; print(platform.python_version())', stdout=asyncio.subprocess.PIPE)
    stdout, _ = await proc.communicate()
    pathlib.Path(os.getenv('GITHUB_ENV')).write_text('PY=' + stdout.decode())

asyncio.run(main())
