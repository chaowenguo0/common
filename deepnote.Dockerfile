FROM ubuntu:22.04
ARG DEBIAN_FRONTEND noninteractive
RUN <<EOF
apt update
apt install -y --no-install-recommends python3-pip curl tzdata
ln /usr/bin/python3 /usr/bin/python
EOF