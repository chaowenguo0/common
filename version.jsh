final var process = new java.lang.ProcessBuilder("docker", "run", "--rm", "openjdk:slim", "jshell", "--version").start();
java.nio.file.Files.write(java.nio.file.Paths.get(java.lang.System.getenv("GITHUB_ENV")), ("JAVA=" + process.inputReader().readLine().split(" ")[1]).getBytes());
