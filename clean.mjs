const headers = {'private-token':process.argv.at(2)}
for (const _ of await globalThis.fetch(`https://gitlab.com/api/v4/projects/${process.env.CI_PROJECT_ID}/pipelines?per_page=100`, headers).then(_ => _.json()).then(_ => _.map(_ => _.id)))
{
    await globalThis.fetch(`https://gitlab.com/api/v4/projects/${process.env.CI_PROJECT_ID}/pipelines/` + _, {method:'delete', headers})
}
