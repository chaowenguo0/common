import child_process from 'child_process'
import process from 'process'
import {promises as fs} from 'fs'

const spawn = child_process.spawn('docker', ['run', '--rm', 'node:slim', 'node', '--version'])
for await (const _ of spawn.stdout) await fs.writeFile(process.env.GITHUB_ENV, 'JS=' + _.toString().slice(1))
